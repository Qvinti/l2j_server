L2J Server Project
===

L2J Server is an open-source server emulator fully written in Java for the famous Korean MMORPG.
---

This page only gives very basic information, for the detailed information about building and developing L2J Server, please visit the developers forums.

- master branch is the latest High Five release

- develop branch is the current High Five development

Legal
---

L2J is distributed under the terms of GNU/GPL, we require you to:

a) Preserve login notice. This gives us, L2J Developers, appropriate
credit for our hard work done during our free time without any
revenues.
 
b) Do not distribute any extended data files with the server files in
the same archive. NO world content should be incorporated in L2J
distribution.
Server and Datapack may not be not be bundled or packaged.

Requeriments
---

Windows or Linux system with Java JDK 11+, Maven 3.5+, MySQL 8.0+.

At least 4gb of RAM and 2 Cores.

Open ports 2016 and 7777.

High Five part 5 NA client fully updated.

Links
---

- [Web Site](http://www.l2jserver.com)

- [Forums](http://www.l2jserver.com/forum/)

- [Live Support](https://gitter.im/L2J/L2J_Server)

- [Downloads](http://www.l2jserver.com/#downloads)

- [Documentation](https://bitbucket.org/l2jserver/l2j_server/wiki)

- [@l2jserver](https://twitter.com/l2jserver)